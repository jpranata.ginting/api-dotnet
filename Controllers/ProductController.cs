﻿using api.Entities;
using api.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private readonly IProductService productService;

        public ProductsController(IProductService productService)
        {
            this.productService = productService;
        }

        [HttpGet("{id}")]
        public async Task<IEnumerable<Product>> GetProductByIdAsync(int Id)
        {
            try
            {
                var response = await productService.GetProductByIdAsync(Id);

                if (response == null)
                {
                    return null;
                }

                return response;
            }
            catch
            {
                throw;
            }
        }

        [HttpGet("list")]
        public async Task<ActionResult<List<Product>>> GetProductListAsync(
            string? title = "",
            string? category = "",
            string? brand = "",
            int pageNumber = 1,
            int pageSize = 100)
        {
            try
            {
                var products = await productService.GetProductListAsync(title, category, brand, pageNumber, pageSize);
                return Ok(products);
            }
            catch
            {
                throw;
            }
        }

        [HttpPost("add")]
        public async Task<IActionResult> AddProductAsync(Product product)
        {
            if (product == null)
            {
                return BadRequest();
            }

            try
            {
                var response = await productService.AddProductAsync(product);

                return Ok(response);
            }
            catch
            {
                throw;
            }
        }

        [HttpPut("update")]
        public async Task<IActionResult> UpdateProductAsync(Product product)
        {
            if (product == null)
            {
                return BadRequest();
            }

            try
            {
                var result = await productService.UpdateProductAsync(product);
                return Ok(result);
            }
            catch
            {
                throw;
            }
        }

        [HttpDelete("delete/{id}")]
        public async Task<int> DeleteProductAsync(int id)
        {
            Console.WriteLine("Received ID: " + id);
            try
            {
                var response = await productService.DeleteProductAsync(id);
                return response;
            }
            catch
            {
                throw;
            }
        }
    }
}