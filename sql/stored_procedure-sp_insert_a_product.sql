USE [products]
GO

/****** Object:  StoredProcedure [dbo].[sp_insert_a_product] ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_insert_a_product] (
	@ProductTitle				VARCHAR (100), 
	@ProductDescription			TEXT,
	@ProductCategory			VARCHAR (100), 
	@ProductBrand				VARCHAR	(100), 
	@ProductPrice				DECIMAL (10, 2), 
	@ProductDiscountPercentage	DECIMAL (5, 2), 
	@ProductStock				BIGINT,
	@ProductThumbnail			VARCHAR (255)
)
AS
BEGIN
	INSERT INTO products (title, description, category, brand, price, discount_percentage, stock, thumbnail)
VALUES (
	@ProductTitle, 
	@ProductDescription,
	@ProductCategory, 
	@ProductBrand, 
	@ProductPrice, 
	@ProductDiscountPercentage, 
	@ProductStock,
	@ProductThumbnail
)
END
GO