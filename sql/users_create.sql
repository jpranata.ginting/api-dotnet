CREATE TABLE users (
	id				INT				NOT NULL	IDENTITY	PRIMARY KEY,
	full_name		VARCHAR (100)	NOT NULL,
	username		VARCHAR (100)	NOT NULL,
	email			VARCHAR (100)	NOT NULL,
	password_hash	BINARY (64)		NOT NULL,
	activated		BIT				NOT NULL,
	CONSTRAINT		U_Email	UNIQUE (email)
);