INSERT INTO products (title, description, price, discount_percentage, stock, brand, category, thumbnail)
VALUES
  ('iPhone 9', 'An apple mobile which is nothing like apple', 549.00, 12.96, 94, 'Apple', 'smartphones', 'https://i.dummyjson.com/data/products/1/thumbnail.jpg'),
  ('iPhone X', 'SIM-Free, Model A19211 6.5-inch Super Retina HD display with OLED technology A12 Bionic chip with ...', 899.00, 17.94, 34, 'Apple', 'smartphones', 'https://i.dummyjson.com/data/products/2/thumbnail.jpg'),
  ('Samsung Universe 9', 'Samsung''s new variant which goes beyond Galaxy to the Universe', 1249.00, 15.46, 36, 'Samsung', 'smartphones', 'https://i.dummyjson.com/data/products/3/thumbnail.jpg'),
  ('OPPOF19', 'OPPO F19 is officially announced on April 2021.', 280.00, 17.91, 123, 'OPPO', 'smartphones', 'https://i.dummyjson.com/data/products/4/thumbnail.jpg'),
  ('Huawei P30', 'Huawei�s re-badged P30 Pro New Edition was officially unveiled yesterday in Germany and now the device has made its way to the UK.', 499.00, 10.58, 32, 'Huawei', 'smartphones', 'https://i.dummyjson.com/data/products/5/thumbnail.jpg'),
  ('MacBook Pro', 'MacBook Pro 2021 with mini-LED display may launch between September, November', 1749.00, 11.02, 83, 'Apple', 'laptops', 'https://i.dummyjson.com/data/products/6/thumbnail.png'),
  ('Samsung Galaxy Book', 'Samsung Galaxy Book S (2020) Laptop With Intel Lakefield Chip, 8GB of RAM Launched', 1499.00, 4.15, 50, 'Samsung', 'laptops', 'https://i.dummyjson.com/data/products/7/thumbnail.jpg'),
  ('Microsoft Surface Laptop 4', 'Style and speed. Stand out on HD video calls backed by Studio Mics. Capture ideas on the vibrant touchscreen.', 1499.00, 10.23, 68, 'Microsoft Surface', 'laptops', 'https://i.dummyjson.com/data/products/8/thumbnail.jpg'),
  ('Dell XPS 13', 'Dell XPS 13 (2021) Laptop With Intel Tiger Lake CPUs, Intel Xe Graphics Launched', 1399.00, 7.36, 45, 'Dell', 'laptops', 'https://i.dummyjson.com/data/products/9/thumbnail.jpg'),
  ('Sony PlayStation 5', 'The Sony PS5 console is powered by an 8-core AMD Zen 2 CPU and a custom AMD RDNA 2-based GPU.', 499.00, 0.00, 12, 'Sony', 'gaming', 'https://i.dummyjson.com/data/products/10/thumbnail.jpg');
