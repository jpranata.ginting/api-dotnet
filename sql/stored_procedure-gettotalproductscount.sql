USE [products]
GO

/****** Object:  StoredProcedure [dbo].[sp_select_products] ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_select_products]
    @TitleSearch     VARCHAR(100) = NULL,
    @CategoryFilter  VARCHAR(100) = NULL,
    @BrandFilter     VARCHAR(100) = NULL,
    @PageNumber          INT = 1,
    @PageSize            INT = 10,
    @TotalProducts       INT OUTPUT
AS
BEGIN
    SELECT @TotalProducts = COUNT(*)
    FROM products
    WHERE (@TitleSearch IS NULL OR title LIKE '%' + @TitleSearch + '%') 
        AND (@CategoryFilter IS NULL OR category = @CategoryFilter OR LEN(@CategoryFilter) = 0) 
        AND (@BrandFilter IS NULL OR brand = @BrandFilter OR LEN(@BrandFilter) = 0) 

    SELECT id, title, description, category, brand, price, discount_percentage, stock, thumbnail 
    FROM products 
    WHERE (@TitleSearch IS NULL OR title LIKE '%' + @TitleSearch + '%') 
        AND (@CategoryFilter IS NULL OR category = @CategoryFilter OR LEN(@CategoryFilter) = 0) 
        AND (@BrandFilter IS NULL OR brand = @BrandFilter OR LEN(@BrandFilter) = 0) 
    ORDER BY title
        OFFSET (@PageNumber - 1) * @PageSize ROWS 
        FETCH NEXT @PageSize ROWS ONLY
END
GO
