INSERT INTO users (full_name, username, email, password_hash, activated)
VALUES (
	'Administrator',
	'admin',
	'admin@localhost.com',
	HASHBYTES('SHA2_256', 'pa55word'),
	1
);