CREATE TABLE images (
    id          INT             NOT NULL IDENTITY PRIMARY KEY,
    product_id  SMALLINT        NOT NULL,
    url         VARCHAR(255)    NOT NULL
);

ALTER TABLE images
ADD CONSTRAINT FK_Images_Products
FOREIGN KEY (product_id)
REFERENCES products (id)
ON DELETE CASCADE;