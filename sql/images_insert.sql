INSERT INTO images (product_id, url)
VALUES
  (1, 'https://i.dummyjson.com/data/products/1/image1.jpg'),
  (1, 'https://i.dummyjson.com/data/products/1/image2.jpg'),
  (1, 'https://i.dummyjson.com/data/products/1/image3.jpg'),
  (2, 'https://i.dummyjson.com/data/products/2/image1.jpg'),
  (2, 'https://i.dummyjson.com/data/products/2/image2.jpg'),
  (3, 'https://i.dummyjson.com/data/products/3/image1.jpg'),
  (3, 'https://i.dummyjson.com/data/products/3/image2.jpg'),
  (4, 'https://i.dummyjson.com/data/products/4/image1.jpg'),
  (5, 'https://i.dummyjson.com/data/products/5/image1.jpg'),
  (5, 'https://i.dummyjson.com/data/products/5/image2.jpg'),
  (6, 'https://i.dummyjson.com/data/products/6/image1.png'),
  (7, 'https://i.dummyjson.com/data/products/7/image1.jpg'),
  (8, 'https://i.dummyjson.com/data/products/8/image1.jpg'),
  (9, 'https://i.dummyjson.com/data/products/9/image1.jpg'),
  (10, 'https://i.dummyjson.com/data/products/10/image1.jpg');