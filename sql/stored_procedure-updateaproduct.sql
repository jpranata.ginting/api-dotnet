USE [products]
GO

/****** Object:  StoredProcedure [dbo].[sp_update_a_product] ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_update_a_product] (
	@ProductId					SMALLINT,
	@ProductTitle				VARCHAR (100), 
	@ProductDescription			TEXT,
	@ProductCategory			VARCHAR (100), 
	@ProductBrand				VARCHAR	(100), 
	@ProductPrice				DECIMAL (10, 2), 
	@ProductDiscountPercentage	DECIMAL (5, 2), 
	@ProductStock				BIGINT,
	@ProductThumbnail			VARCHAR (255)
)
AS
BEGIN
	UPDATE products
SET	title				= @ProductTitle, 
	description			= @ProductDescription,
	category			= @ProductCategory, 
	brand				= @ProductBrand, 
	price				= @ProductPrice, 
	discount_percentage	= @ProductDiscountPercentage, 
	stock				= @ProductStock,
	updated_at			= current_timestamp,
	version				= version + 1
WHERE id = @ProductId
END
GO