USE [products]
GO

/****** Object:  StoredProcedure [dbo].[GetProductById] ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_select_a_product]
@ProductId int
AS
BEGIN
	SELECT * FROM dbo.Products WHERE id = @ProductId
END
GO