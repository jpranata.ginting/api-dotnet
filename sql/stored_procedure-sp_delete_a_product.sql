USE [products]
GO

/****** Object:  StoredProcedure [dbo].[sp_delete_a_product] ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_delete_a_product] (
	@ProductId					SMALLINT
)
AS
BEGIN
	DELETE FROM products
	WHERE id = @ProductId
END
GO