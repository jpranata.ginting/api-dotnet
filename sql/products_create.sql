CREATE TABLE products (
	id					SMALLINT		NOT NULL IDENTITY PRIMARY KEY,
	title				VARCHAR (100)	NOT NULL,
	description			TEXT			NOT NULL,
	category			VARCHAR (100)	NOT NULL,
	brand				VARCHAR	(100)	NOT NULL,
	price				DECIMAL (10, 2)	NOT NULL,
	discount_percentage	DECIMAL (5, 2)	NOT NULL,
	stock				BIGINT			NOT NULL,
	thumbnail			VARCHAR (255)	NOT NULL,
	created_at			DATETIME		DEFAULT current_timestamp	NOT NULL,
	updated_at			DATETIME,
	version				SMALLINT		NOT NULL	DEFAULT 1
);

CREATE NONCLUSTERED INDEX Index_Products_Category_Brand
ON Products (category, brand);