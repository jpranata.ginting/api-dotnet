﻿using api.Entities;

public class PaginatedProductList
{
    public List<Product> Products { get; set; }
    public int TotalProducts { get; set; }
    public int PageNumber { get; set; }
    public int PageSize { get; set; }
}
