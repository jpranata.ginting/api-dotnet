﻿namespace api.Entities
{
    public class Product
    {
        public Int16 Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Category { get; set; }
        public string Brand { get; set; }
        public Decimal Price { get; set; }
        public Decimal Discount_Percentage { get; set; }
        public Int64 Stock { get; set; }
        public string Thumbnail { get; set; }

        /*
         
        // Create gitlab account and repo

        // Store Procedure improvement

        // Additional Properties
        CreatedAt
        UpdatedAt
        Version

        // Pagination
        search: implement search by title

        * optional
        sort: sort by title, etc

        total: total records of products
        - use count in sql

        limit: total records being included in a response
        - avoid using limit by. it's slow.
        
         * optional
        skip: sum of records that not being showed
        - limit times current oagination number

        */

    }
}
