import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss'],
})
export class ProductDetailComponent implements OnInit {
  productId: number;
  responseProduct: any[];
  product: any;
  // imageUrls: string[];
  // currentImageIndex: number;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private http: HttpClient
  ) {
    this.productId = 0;
    this.responseProduct = [];
    this.product = {
      id: 0,
      title: '',
      description: '',
      price: 0,
      discount_percentage: 0.0,
      stock: 0,
      brand: '',
      category: '',
      thumbnail: '',
      // images: [],
    };
    // this.imageUrls = [];
    // this.currentImageIndex = 0;
  }

  ngOnInit() {
    this.route.params.subscribe((params) => {
      this.productId = +params['id'];
    });

    this.http.get<any>(`https://localhost:7183/api/products/${this.productId}`).subscribe(
      (response) => {
        this.responseProduct = response;
        this.product = this.responseProduct[0];
        // this.imageUrls = this.product.thumbnail;
      },
      (error) => {
        console.error(error);
      }
    );
  }

  /*showNextImage() {
    this.currentImageIndex =
      (this.currentImageIndex + 1) % this.imageUrls.length;
  }

  showPreviousImage() {
    this.currentImageIndex =
      (this.currentImageIndex - 1 + this.imageUrls.length) %
      this.imageUrls.length;
  }*/

  back(): void {
    this.router.navigate(['/']);
  }
}
