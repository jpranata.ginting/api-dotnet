import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-product-new',
  templateUrl: './product-new.component.html',
  styleUrls: ['./product-new.component.scss'],
})
export class ProductNewComponent {
  categories: string[];
  product: any;

  constructor(private router: Router, private http: HttpClient) {
    this.categories = [];
    this.product = {
      id: 0,
      title: '',
      description: '',
      price: 0,
      discountPercentage: 0.0,
      stock: 0,
      brand: '',
      category: '',
    };
  }

  ngOnInit() {
    /* this.http
      .get<string[]>('https://dummyjson.com/products/categories')
      .subscribe(
        (response) => {
          this.categories = response;
          console.log(this.categories);
        },
        (error) => {
          console.error(error);
        }
      ); */

      this.categories = [
        "Smartphones",
        "Laptop",
        "Etc"
      ]
  }

  onSubmit() {
    const url = 'https://localhost:7183/api/products/add';
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });

    this.http.post(url, this.product, { headers }).subscribe((res) => {
      console.log(res);
    });
  }

  back(): void {
    this.router.navigate(['..']);
  }
}
