import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGuard, AuthGuardForLogin } from './login/auth.guard';

import { LoginComponent } from './login/login.component';
import { ProductListComponent } from './product-list/product-list.component';
import { ProductDetailComponent } from './product-detail/product-detail.component';
import { ProductEditComponent } from './product-edit/product-edit.component';
import { ProductNewComponent } from './product-new/product-new.component';

/* const routes: Routes = [
  { path: '', component: ProductListComponent },
  {
    path: 'login',
    canActivate: [AuthGuardForLogin],
    children: [{ path: '', component: LoginComponent }],
  },
  { path: 'new', canActivate: [AuthGuard], component: ProductNewComponent },
  {
    path: ':id',
    canActivate: [AuthGuard],
    children: [
      { path: '', component: ProductDetailComponent },
      { path: 'edit', component: ProductEditComponent },
      { path: 'delete', component: ProductEditComponent },
    ],
  },
]; */

const routes: Routes = [
  { path: '', component: ProductListComponent },
  /* {
    path: 'login',
   
    children: [{ path: '', component: LoginComponent }],
  }, */
  { path: 'new',  component: ProductNewComponent },
  {
    path: ':id',
   
    children: [
      { path: '', component: ProductDetailComponent },
      { path: 'edit', component: ProductEditComponent },
      { path: 'delete', component: ProductEditComponent },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
