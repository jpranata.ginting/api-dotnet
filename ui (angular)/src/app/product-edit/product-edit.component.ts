import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-edit-product',
  templateUrl: './product-edit.component.html',
  styleUrls: ['./product-edit.component.scss'],
})
export class ProductEditComponent implements OnInit {
  productId: number;
  product: any;
  responseProduct: any[];
  // categories: string[];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private http: HttpClient
  ) {
    this.productId = 0;
    this.responseProduct = [];
    this.product = {};
    // this.categories = [];
  }

  ngOnInit() {
    this.route.params.subscribe((params) => {
      this.productId = +params['id'];
    });
    /* this.http
      .get<string[]>('https://dummyjson.com/products/categories')
      .subscribe(
        (response) => {
          this.categories = response;
          console.log(this.categories);
        },
        (error) => {
          console.error(error);
        }
      );*/

    this.http.get<any>(`https://localhost:7183/api/products/${this.productId}`).subscribe(
      (response) => {
        this.responseProduct = response[0];
        this.product = this.responseProduct;
      },
      (error) => {
        console.error(error);
      }
    );
  }

  onSubmit() {
    this.http
      .put(`https://localhost:7183/api/products/update`, this.product)
      .subscribe(
        (response) => {
          console.log(response);
          this.router.navigate(['/product-detail', this.productId]);
        },
        (error) => {
          console.error(error);
        }
      );
  }

  back(): void {
    this.router.navigate(['..']);
  }
}
