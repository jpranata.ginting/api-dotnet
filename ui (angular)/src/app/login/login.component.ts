import { Component } from '@angular/core';
import { AuthService } from './auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent {
  isLoggedIn: boolean;
  username: string;
  password: string;
  status: string;
  subtle: string;
  heading: string;

  constructor(private authService: AuthService, private router: Router) {
    this.isLoggedIn = this.authService.isAuthenticated();
    this.username = '';
    this.password = '';
    this.status = 'Log In';
    this.heading = 'Welcome back!';
    this.subtle = 'Please login to modify the products information.';
  }

  onSubmit() {
    this.status = 'Logging In';
    this.authService.login(this.username, this.password).subscribe(
      (response) => {
        this.status = 'Login Successful!';
        this.heading = 'Wait!';
        this.subtle = 'You are being redirected to the homepage.';
        const authToken = response.token;
        localStorage.setItem('authToken', authToken);
        localStorage.setItem(
          'user',
          JSON.stringify({
            username: response.username,
            email: response.email,
            fullName: `${response.firstName} ${response.lastName}`,
          })
        );
        this.isLoggedIn = true;
        this.router.navigate(['/']);
        location.reload();
      },
      (subtle) => {
        this.status = 'Log In';
        this.heading = 'Uups!';
        this.subtle = 'Wrong username or password! Please try again';
      }
    );
  }

  back(): void {
    this.router.navigate(['..']);
  }
}
