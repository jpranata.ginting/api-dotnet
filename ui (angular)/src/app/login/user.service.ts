import { Injectable } from '@angular/core';

@Injectable()
export class UserService {
  getUserData() {
    const userData = localStorage.getItem('user');
    if (userData) {
      return JSON.parse(userData);
    }

    return {};
  }
}
