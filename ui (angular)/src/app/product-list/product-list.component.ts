import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ProductListService } from './product-list.service';
import { Data, Product } from './product.model';

import { AuthService } from '../login/auth.service';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss'],
})
export class ProductListComponent implements OnInit {
  isAuthenticated: boolean;
  data: Data;
  products: Product[];
  filteredProducts: Product[];
  currentPage: number;
  pageSize: number;
  totalProducts: number;
  totalPages: number;
  limitCharacters: number;
  pageNumbers: number[];
  searchKeyword: string;
  showDeleteConfirmation: boolean;
  productToDelete: any;
  productToDeleteName: string;

  constructor(
    private authService: AuthService,
    private http: HttpClient,
    private productListService: ProductListService
  ) {
    this.data = {
      limit: 0,
      products: [],
      skip: 0,
      total: 0,
    };
    this.isAuthenticated = false;
    this.products = [];
    this.filteredProducts = [];
    this.currentPage = 1;
    this.pageSize = 10;
    this.totalProducts = 100;
    this.totalPages = 3;
    this.limitCharacters = 50;
    this.pageNumbers = [];
    this.searchKeyword = '';
    this.showDeleteConfirmation = false;
    this.productToDelete = {};
    this.productToDeleteName = '';
  }

  ngOnInit() {
    this.isAuthenticated = this.authService.isAuthenticated();
    this.loadProducts();
  }

  loadProducts() {
    this.productListService
      .getProducts(/*this.currentPage, this.pageSize*/)
      .subscribe((data) => {
        this.products = data;
        console.log(data);
        /*this.filteredProducts = data.products;
        this.totalProducts = data.total;
        this.totalPages = this.totalProducts / this.pageSize;*/
      });
  }

  /*
  onSearchInputChange() {
    this.filteredProducts = this.products.filter((product) =>
      product.title.toLowerCase().includes(this.searchKeyword.toLowerCase())
    );
  }

  onSearchButtonClick() {
    fetch(`https://dummyjson.com/products/search?q=${this.searchKeyword}`)
      .then((res) => res.json())
      .then((data) => {
        this.filteredProducts = data.products;
        this.totalProducts = data.total;
        this.pageSize = data.limit;
        this.totalPages = this.totalProducts / this.pageSize;
      });
  }

  onPageChange(newPage: number) {
    this.currentPage = newPage;
    this.loadProducts();
  }

  getPageNumbers(): number[] {
    return Array.from({ length: this.totalPages }, (_, i) => i + 1);
  }

  goToPage(page: number) {
    if (page >= 1 && page <= this.totalPages) {
      this.onPageChange(page);
    }
  }*/

  onDelete(productId: number, productName: string) {
    /*if (this.isAuthenticated) {
      this.productToDelete = this.products.find(
        (product) => product.id === productId
      );
      this.showDeleteConfirmation = true;
      this.productToDeleteName = productName;
    } else {
      console.log(
        "You're not allowed to modify this resource. Please login first!"
      );
    }*/

    this.productToDelete = this.products.find(
      (product) => product.id === productId
    );
    this.showDeleteConfirmation = true;
    this.productToDeleteName = productName;
  }

  confirmDelete() {
    const url = `https://localhost:7183/api/products/delete/${this.productToDelete.id}`;
    this.http.delete(url).subscribe((res) => {
      console.log(res);
      this.showDeleteConfirmation = false;
    });
    const index = this.products.findIndex(
      (product) => product.id === this.productToDelete.id
    );
    if (index > -1) {
      this.products.splice(index, 1);
    }
  }

  cancelDelete() {
    this.showDeleteConfirmation = false;
  }
}
