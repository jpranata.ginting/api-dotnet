import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
// import { Data } from './product.model';

@Injectable()
export class ProductListService {
  private apiUrl: string = 'https://localhost:7183/api/products/list';

  constructor(private http: HttpClient) {}

  getProducts(/*page: number, pageSize: number*/): Observable<any> {
    // const skip = (page - 1) * pageSize;
    // const url = `${this.apiUrl}?skip=${skip}&limit=${pageSize}`;
    return this.http.get<any>(this.apiUrl);
  }
}
