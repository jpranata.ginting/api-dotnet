import { Component } from '@angular/core';
import { AuthService } from './login/auth.service';
import { UserService } from './login/user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  public username: string;
  public email: string;
  public fullName: string;

  constructor(
    public authService: AuthService,
    public userService: UserService
  ) {
    this.username = '';
    this.email = '';
    this.fullName = '';
  }

  ngOnInit(): void {
    const user = this.userService.getUserData();
    if (user) {
      this.username = user.username;
      this.email = user.email;
      this.fullName = user.fullName;
    }
  }

  onLogout() {
    this.authService.logout();
  }
}
