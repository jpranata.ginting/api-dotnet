# INVENTORY

Run:
`
ng serve
`

Check in: http://localhost:4200/

#### Dummy User

username: kminchelle

password: 0lelplR

#### Design System

- Single Primary Color: #0089CF
- Spacing Layout: 4px

#### API

This application developed using `https://localhost:7183/api/products/` API.

#### Product Management System

This is an Angular application for managing products. It includes functionality for listing, viewing, editing, deleting, and adding products. It also includes authentication and authorization to protect certain routes based on user's authentication status.

#### Features

- Product list: Displays a list of products with basic information like name, price, and thumbnail.
- Product detail: Allows users with appropriate permissions to view detailed information of a product, including name, description, price, and image.
- Product edit: Allows users with appropriate permissions to edit the details of a product, including its name, description, and price.
- Product delete: Allows users with appropriate permissions to delete a product from the system.
- Product new: Allows users with appropriate permissions to add a new product to the system.
- Authentication: Provides a login functionality to authenticate users with a backend service.
- Authorization: Uses Angular guards to protect certain routes and components based on the user's authentication status.

#### Technologies Used

- Angular: version 15.2.6
- JSON Web Tokens (JWT) for authentication
- Local Storage for storing authentication token

#### Usage

- Product List: Access the product list by navigating to the homepage. You can view the list of products with basic information. Click on a product to view its details. You need to be authenticated and have appropriate permissions to access this functionality.
- Product Detail: View detailed information of a product by clicking on a product in the product list. If you are authenticated and have appropriate permissions, you can also edit or delete the product.
- Product Edit: Edit the details of a product by clicking on the "Edit" button in the product detail page. You need to be authenticated and have appropriate permissions to access this functionality.
- Product Delete: Delete a product by clicking on the "Delete" button in the product detail page. You need to be authenticated and have appropriate permissions to access this functionality.
- Product New: Add a new product by navigating to the "New Product" page from the navigation menu. You need to be authenticated and have appropriate permissions to access this functionality.
