﻿using api.Data;
using api.Entities;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System.Data;

namespace api.Repositories
{
    public class ProductService : IProductService
    {
        private readonly DbContextClass _dbContext;

        public ProductService(DbContextClass dbContext)
        {
            _dbContext = dbContext;
        }

         

            public async Task<List<Product>> GetProductListAsync(string? title, string? category, string? brand, int? pageNumber, int? pageSize)
            {
                var parameter = new List<SqlParameter>();
                parameter.Add(new SqlParameter("@TitleSearch", title));
                parameter.Add(new SqlParameter("@CategoryFilter", category));
                parameter.Add(new SqlParameter("@BrandFilter", brand));
                parameter.Add(new SqlParameter("@PageNumber", pageNumber));
                parameter.Add(new SqlParameter("@PageSize", pageSize));
                var totalProductsParam = new SqlParameter("@TotalProducts", SqlDbType.Int)
                {
                    Direction = ParameterDirection.Output
                };
                parameter.Add(totalProductsParam);

                var products = await _dbContext.Product
                    .FromSqlRaw<Product>("sp_select_products @TitleSearch, @CategoryFilter, @BrandFilter, @PageNumber, @PageSize, @TotalProducts OUTPUT", parameter.ToArray())
                    .ToListAsync();

                var totalProducts = (int)totalProductsParam.Value;

                return products;
            }


    public async Task<IEnumerable<Product>> GetProductByIdAsync(int Id)
        {
            var parameter = new SqlParameter("@Id", Id);

            var product = await Task.Run(() => _dbContext.Product
                            .FromSqlRaw(@"exec sp_select_a_product @Id", parameter).ToListAsync());

            return product;
        }

        public async Task<int> AddProductAsync(Product product)
        {
            var parameter = new List<SqlParameter>();
            parameter.Add(new SqlParameter("@Title", product.Title));
            parameter.Add(new SqlParameter("@Description", product.Description));
            parameter.Add(new SqlParameter("@Category", product.Category));
            parameter.Add(new SqlParameter("@Brand", product.Brand));
            parameter.Add(new SqlParameter("@Price", product.Price));
            parameter.Add(new SqlParameter("@DiscountPercentage", product.Discount_Percentage));
            parameter.Add(new SqlParameter("@Stock", product.Stock));
            parameter.Add(new SqlParameter("@Thumbnail", product.Thumbnail));

            var result = await Task.Run(() => _dbContext.Database
           .ExecuteSqlRawAsync(@"exec sp_insert_a_product @Title, @Description, @Category, @Brand, @Price, @DiscountPercentage, @Stock, @Thumbnail", parameter.ToArray()));

            return result;
        }

        public async Task<int> UpdateProductAsync(Product product)
        {
            var parameter = new List<SqlParameter>();
            parameter.Add(new SqlParameter("@Id", product.Id));
            parameter.Add(new SqlParameter("@Title", product.Title));
            parameter.Add(new SqlParameter("@Description", product.Description));
            parameter.Add(new SqlParameter("@Category", product.Category));
            parameter.Add(new SqlParameter("@Brand", product.Brand));
            parameter.Add(new SqlParameter("@Price", product.Price));
            parameter.Add(new SqlParameter("@DiscountPercentage", product.Discount_Percentage));
            parameter.Add(new SqlParameter("@Stock", product.Stock));
            parameter.Add(new SqlParameter("@Thumbnail", product.Thumbnail));

            var result = await Task.Run(() => _dbContext.Database
            .ExecuteSqlRawAsync(@"exec sp_update_a_product @Id, @Title, @Description, @Category, @Brand, @Price, @DiscountPercentage, @Stock, @Thumbnail", parameter.ToArray()));
            return result;
        }
        public async Task<int> DeleteProductAsync(int Id)
        {
            return await Task.Run(() => _dbContext.Database.ExecuteSqlInterpolatedAsync($"sp_delete_a_product {Id}"));
        }
    }
}