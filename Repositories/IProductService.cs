﻿using api.Entities;

namespace api.Repositories
{
    public interface IProductService
    {
        public Task<List<Product>> GetProductListAsync(string? title, string? category, string? brand, int? pageNumber, int? pageSize);
        public Task<IEnumerable<Product>> GetProductByIdAsync(int Id);
        public Task<int> AddProductAsync(Product product);
        public Task<int> UpdateProductAsync(Product product);
        public Task<int> DeleteProductAsync(int Id);
    }
}
